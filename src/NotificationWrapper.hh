#pragma once

#include <libnotify/notification.h>
#include <libnotify/notify.h>
#include <string>

class NotificationWrapper {
 private:
  constexpr static const char *const appName = "New Updates";
  constexpr static const char *const category = "update";
  constexpr static const int ONE_HOUR_IN_MILLISECONDS = 3600000;

  class NotificationWrapperProperties {
   private:
    NotificationWrapperProperties()
        : urgency(NOTIFY_URGENCY_NORMAL),
          notifyTimeout(ONE_HOUR_IN_MILLISECONDS),
          icon(nullptr) {}

    NotifyUrgency urgency = NOTIFY_URGENCY_NORMAL;
    long notifyTimeout;
    gchar *icon = nullptr;

    friend class NotificationWrapper;

    friend class NotificationWrapperBuilder;
  };

 public:
  class NotificationWrapperBuilder {
   public:
    NotificationWrapperBuilder &withUrgency(const char *urgency);

    NotificationWrapperBuilder &withNotifyTimeout(long notifyTimeout);

    NotificationWrapperBuilder &withIcon(gchar *icon);

    NotificationWrapper Build();

   private:
    NotificationWrapperProperties properties;
  };

  virtual ~NotificationWrapper();

  gboolean showNotification(const std::string &body,
                            bool forceReinitialize = false);

  gboolean closeNotification();

  GError *getError();

 private:
  explicit NotificationWrapper(const NotificationWrapperProperties &properties)
      : properties(properties) {}

  NotificationWrapperProperties properties;
  NotifyNotification *my_notify = nullptr;
  GError *error = nullptr;

  void initialize(bool forceReinitialize);
};
