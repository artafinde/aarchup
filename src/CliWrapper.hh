#pragma once

static const int BUFFER_SIZE = 128;

#include <array>
#include <cstdio>
#include <iostream>
#include <memory>
#include <sstream>
#include <stdexcept>
#include <string>

class CliWrapper {
  const char *_cliCommand;

 public:
  explicit CliWrapper(const char *cliCommand);

  std::string execute();

  virtual ~CliWrapper();

 private:
  std::string parseOutput(const std::shared_ptr<FILE> &pipe) const;
};