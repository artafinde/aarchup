#pragma once

#include <string>
#include <vector>
#include <unistd.h>
#include "CliWrapper.hh"

class UpdateChecker {
 private:
  constexpr static const char *const aurHeader = "AUR updates:\n";

  class UpdateCheckerProperties {
   private:
    UpdateCheckerProperties()
        : command("/usr/bin/checkupdates"),
          aurCommand("/usr/bin/auracle outdated"),
          maxNumberOfLines(30),
          uid(-1),
          checkAur(false) {}

    const char *command;
    const char *aurCommand;
    long maxNumberOfLines;
    long uid;
    bool checkAur;

    friend class UpdateChecker;

    friend class UpdateCheckerBuilder;
  };

 public:
  class UpdateCheckerBuilder {
   public:
    UpdateCheckerBuilder &withCommand(const char *command);

    UpdateCheckerBuilder &withAurCommand(const char *aurCommand);

    UpdateCheckerBuilder &withMaxNumberOfLines(long maxNumberOfLines);

    UpdateCheckerBuilder &withUid(long Uid);

    UpdateCheckerBuilder &withCheckAur(bool checkAur);

    UpdateChecker Build();

   private:
    UpdateCheckerProperties properties;
  };

  std::string getPendingUpdates(bool forceCheck = false);

  virtual ~UpdateChecker();

 private:
  explicit UpdateChecker(const UpdateCheckerProperties &properties)
      : properties(properties) {}

  void checkForUpdates(bool forceFetch);

  static std::vector<std::string> split(const std::string &s, char delimiter);

  UpdateCheckerProperties properties;
  bool updatesNotFetched = true;
  std::string checkUpdateOut;
  std::string aurHelperOut;
};
