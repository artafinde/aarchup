#include <getopt.h>
#include <unistd.h>
#include <cctype>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <memory>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include "NotificationWrapper.hh"
#include "UpdateChecker.hh"


#define VERSION_NUMBER "2.1.0"

int print_help() {
  std::cout
      << "Usage: aarchup [options]\n\n"
         "Options:\n"
         "          --command|-c [value]        Set the command which gives out the list of updates.\n"
         "                                      The default is '/usr/bin/checkupdates'\n"
         "          --aurcommand|-a [value]     Set the command for AUR checking for updates.\n"
         "                                      The default is '/usr/bin/auracle outdated'\n"
         "          --icon|-p [value]           Shows the icon, whose path has been given as value, in the notification.\n"
         "                                      By default no icon is shown.\n"
         "          --maxentries|-m [value]     Set the maximum number of packages which shall be displayed in the notification.\n"
         "                                      The default value is 30.\n"
         "          --timeout|-t [value]        Set the timeout after which the notification disappears in seconds.\n"
         "                                      The default value is 3600 seconds, which is 1 hour.\n"
         "          --uid|-i [value]            Set the uid of the process.\n"
         "                                      The default is to keep the uid of the user who started aarchup.\n"
         "                                      !!You should change this if root is executing aarchup!!\n"
         "          --urgency|-u [value]        Set the libnotify urgency-level. Possible values are: low, normal and critical.\n"
         "                                      The default value is normal. With changing this value you can change the color of the notification.\n"
         "          --loop-time|-l [value]      Using this the program will loop indefinitely. Just use this if you won't use cron.\n"
         "                                      The value should be the number of minutes from each update check, if none is informed 60 will be used.\n"
         "                                      For more information on it check man.\n"
         "          --help|-h                   Prints this help.\n"
         "          --version|-v                Shows the version.\n"
         "          --aur                       Check aur for new packages too. Will need AUR helper installed, defaults to 'auracle'\n"
         "          --debug|-d                  Print debug info.\n"
         "          --ftimeout|-f [value]       Program will manually enforce timeout for closing notification.\n"
         "                                      Do NOT use with --timeout, if --timeout works or without --loop-time [value].\n"
         "                                      The value for this option should be in minutes.\n"
         "\nMore information can be found in the manpage.\n";
  exit(0);
}

int print_version() {
  std::cout
      << "aarchup " VERSION_NUMBER
         "\nCopyright 2019 Leonidas Spyropoulos <artafinde@gmail.com>\n"
         "License GPLv3+: GNU GPL version 3 or later "
         "<http://gnu.org/licenses/gpl.html>.\n"
         "This is free software, you are free to change and redistribute it.\n"
         "There is NO WARRANTY, to the extent permitted by law.\n";
  exit(0);
}

int main(int argc, char **argv) {
  long loop_time = 3600;
  long manual_timeout = 0;
  bool will_loop = FALSE;

  auto console = spdlog::stdout_color_mt("console");
  spdlog::set_default_logger(console);

  if (argc > 1) {
    if (strcmp(argv[1], "--version") == 0 || strcmp(argv[1], "-v") == 0) {
      print_version();
    }
    if (strcmp(argv[1], "--help") == 0 || strcmp(argv[1], "-h") == 0) {
      print_help();
    }
  }

  UpdateChecker::UpdateCheckerBuilder updateCheckerBuilder =
      UpdateChecker::UpdateCheckerBuilder();
  NotificationWrapper::NotificationWrapperBuilder notificationWrapperBuilder =
      NotificationWrapper::NotificationWrapperBuilder();
  const char *const short_opts = "c:a:p:m:t:i:u:l:df:";
  auto version_flag = 0;
  auto help_flag = 0;
  auto aur = 0;
  const option long_opts[] = {
      {"command", required_argument, nullptr, 'c'},
      {"aurcommand", required_argument, nullptr, 'a'},
      {"icon", required_argument, nullptr, 'p'},
      {"maxentries", required_argument, nullptr, 'm'},
      {"timeout", required_argument, nullptr, 't'},
      {"uid", required_argument, nullptr, 'i'},
      {"urgency", required_argument, nullptr, 'u'},
      {"loop-time", required_argument, nullptr, 'l'},
      {"help", no_argument, &help_flag, 1},
      {"version", no_argument, &version_flag, 1},
      {"aur", no_argument, &aur, 1},
      {"ftimeout", required_argument, nullptr, 'f'},
      {"debug", no_argument, nullptr, 'd'},
      {nullptr, 0, nullptr, 0},
  };

  while (true) {
    int option_index = 0;
    const auto opt =
        getopt_long(argc, argv, short_opts, long_opts, &option_index);
    if (-1 == opt) {
      break;
    }
    switch (opt) {
      case 'd':
        spdlog::set_level(spdlog::level::debug);
        break;
      case 0:
        if (long_opts[option_index].flag) {
          break;
        }
        if (optarg) {
          spdlog::error("Undefined option {}  with args {}", long_opts[option_index].name, optarg);
        } else {
          spdlog::error("Undefined option {}", long_opts[option_index].name);
        }
        break;
      case 'v':
        spdlog::debug("Printing version");
        print_version();
        break;
      case 'c':
        updateCheckerBuilder.withCommand(optarg);
        spdlog::debug("Command set: {}", optarg);
        break;
      case 'a':
        updateCheckerBuilder.withAurCommand(optarg);
        spdlog::debug("AUR command set: {}", optarg);
        break;
      case 'p':
        notificationWrapperBuilder.withIcon(optarg);
        spdlog::debug("Icon set: {} lines", optarg);
        break;
      case 'm':
        if (!isdigit(optarg[0])) {
          spdlog::critical("Argument '--maxentries' should be number");
          exit(1);
        }
        updateCheckerBuilder.withMaxNumberOfLines(std::stol(optarg));
        spdlog::debug("Max_number set: {} lines", std::stol(optarg));
        break;
      case 't':
        if (!isdigit(optarg[0])) {
          spdlog::critical("Argument '--timeout' should be number");
          exit(1);
        } else {
          auto notifyTimeout = std::stol(optarg) * 1000;
          notificationWrapperBuilder.withNotifyTimeout(notifyTimeout);
          spdlog::debug("Timeout set: {} msec(s)", notifyTimeout);
        }
        break;
      case 'i':
        if (!isdigit(optarg[0])) {
          spdlog::critical("Argument '--uid' should be number");
          exit(1);
        }
        updateCheckerBuilder.withUid(std::stol(optarg));
        spdlog::debug("Setting uid to: {}", std::stol(optarg));
        break;
      case 'u':
        notificationWrapperBuilder.withUrgency(optarg);
        spdlog::debug("Urgency set: {}", optarg);
        break;
      case 'l':
        will_loop = TRUE;
        if (!isdigit(optarg[0])) {
          spdlog::critical("Argument '--loop-time' should be number");
          exit(1);
        }
        loop_time = std::stol(optarg) * 60;
        spdlog::debug("Loop_time set: {} min(s)", loop_time / 60);
        break;
      case 'f':
        if (!isdigit(optarg[0])) {
          spdlog::critical("Argument '--ftimeout' should be number");
          exit(1);
        }
        manual_timeout = std::stol(optarg) * 60;
        if (!will_loop) {
          spdlog::critical("Argument '--ftimeout' can't be used without or before '--loop-time'");
          exit(1);
        }
        if (manual_timeout > loop_time) {
          spdlog::critical("Please set a value for '--ftimeout' that is lower than '--loop-time'");
          exit(1);
        }
        spdlog::debug("Manual_timeout: {} min(s)", manual_timeout / 60);
        break;
      case 'h':
      case '?':
        print_help();
        break;
      default:
        exit(1);
    }
  }

  if (aur == 1) {
    updateCheckerBuilder.withCheckAur(true);
    spdlog::debug("Enabled AUR updates checking");
  }

  auto notificationWrapper = notificationWrapperBuilder.Build();
  long offset = 0;
  do {
    auto pendingUpdates = updateCheckerBuilder.Build().getPendingUpdates();
    if (pendingUpdates.length() > 0) {
      gboolean success;
      success = notificationWrapper.showNotification(pendingUpdates);
      if (success) {
        spdlog::debug("Notification shown successfully");
      } else {
        GError *error = notificationWrapper.getError();
        spdlog::warn("Failed to close, reason:\n\t[{}] {}", error->code, error->message);
        g_error_free(error);
      }
      if (manual_timeout && success && will_loop) {
        spdlog::debug("Will close notification in {} minutes (this time will be reduced from the loop-time)", manual_timeout / 60);
        sleep(static_cast<unsigned int>(manual_timeout));
        offset = manual_timeout;
        if (notificationWrapper.closeNotification())
          spdlog::debug("Notification closed");
        else {
          auto error = notificationWrapper.getError();
          spdlog::warn("Failed to close, reason:\n\t[{}] {}", error->code, error->message);
        }
      }
    } else {
      spdlog::info("No updates found");
      if (notificationWrapper.closeNotification())
        spdlog::debug("Notification closed");
      else {
        auto error = notificationWrapper.getError();
        spdlog::warn("Failed to close, reason:\n\t[{}] {}", error->code, error->message);
      }
    }

    if (will_loop) {
      spdlog::debug("Next run will be in {} minutes", (loop_time - offset) / 60);
      sleep(static_cast<unsigned int>(loop_time - offset));
      offset = 0;
    }
  } while (will_loop);
  return 0;
}
