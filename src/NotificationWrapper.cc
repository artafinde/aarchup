#include "NotificationWrapper.hh"

NotificationWrapper::~NotificationWrapper() = default;

NotificationWrapper NotificationWrapper::NotificationWrapperBuilder::Build() {
  return NotificationWrapper(properties);
}

NotificationWrapper::NotificationWrapperBuilder &
NotificationWrapper::NotificationWrapperBuilder::withIcon(gchar *icon) {
  this->properties.icon = icon;
  return *this;
}

NotificationWrapper::NotificationWrapperBuilder &
NotificationWrapper::NotificationWrapperBuilder::withNotifyTimeout(long notifyTimeout) {
  this->properties.notifyTimeout = notifyTimeout;
  return *this;
}

NotificationWrapper::NotificationWrapperBuilder &
NotificationWrapper::NotificationWrapperBuilder::withUrgency(const char *urgency) {
  if (strcmp(urgency, "low") == 0) {
    this->properties.urgency = NOTIFY_URGENCY_LOW;
  } else if (strcmp(urgency, "critical") == 0) {
    this->properties.urgency = NOTIFY_URGENCY_CRITICAL;
  }
  return *this;
}

void NotificationWrapper::initialize(bool forceReinitialize) {
  if (notify_is_initted() && forceReinitialize) {
    my_notify = nullptr;
    notify_uninit();
  }
  if (!notify_is_initted()) {
    notify_init(appName);
  }
  my_notify = notify_notification_new("New updates for Arch Linux available!", "", properties.icon);
  notify_notification_set_timeout(my_notify, properties.notifyTimeout);
  notify_notification_set_category(my_notify, category);
  notify_notification_set_urgency(my_notify, properties.urgency);

}

gboolean NotificationWrapper::showNotification(const std::string &body, bool forceReinitialize) {
  if (!my_notify || forceReinitialize) {
    this->initialize(forceReinitialize);
  }
  notify_notification_update(my_notify, "New updates for Arch Linux available!", body.c_str(), properties.icon);
  return notify_notification_show(my_notify, &error);
}

GError *NotificationWrapper::getError() {
  return error;
}

gboolean NotificationWrapper::closeNotification() {
  if (my_notify && error == nullptr) {
    return notify_notification_close(my_notify, &error);
  } else if (my_notify == nullptr) {
    return true;
  }
  return false;
}
