#include "UpdateChecker.hh"

UpdateChecker::~UpdateChecker() = default;

UpdateChecker UpdateChecker::UpdateCheckerBuilder::Build() {
  return UpdateChecker(properties);
}

UpdateChecker::UpdateCheckerBuilder &
UpdateChecker::UpdateCheckerBuilder::withCommand(const char *command) {
  this->properties.command = command;
  return *this;
}

UpdateChecker::UpdateCheckerBuilder &
UpdateChecker::UpdateCheckerBuilder::withAurCommand(const char *aurCommand) {
  this->properties.aurCommand = aurCommand;
  return *this;
}

UpdateChecker::UpdateCheckerBuilder &
UpdateChecker::UpdateCheckerBuilder::withMaxNumberOfLines(
    long maxNumberOfLines) {
  this->properties.maxNumberOfLines = maxNumberOfLines;
  return *this;
}

UpdateChecker::UpdateCheckerBuilder &
UpdateChecker::UpdateCheckerBuilder::withUid(long Uid) {
  this->properties.uid = Uid;
  return *this;
}

UpdateChecker::UpdateCheckerBuilder &
UpdateChecker::UpdateCheckerBuilder::withCheckAur(bool checkAur) {
  this->properties.checkAur = checkAur;
  return *this;
}

void UpdateChecker::checkForUpdates(bool forceFetch) {
  if (properties.uid >= 0) {
    if (setuid(static_cast<__uid_t>(properties.uid)) != 0) {
      exit(1);
    }
  }
  if (!updatesNotFetched || forceFetch) {
    auto checkUpdatesCmd = std::make_unique<CliWrapper>(properties.command);
    this->checkUpdateOut = checkUpdatesCmd->execute();
    if (properties.checkAur && properties.aurCommand != nullptr) {
      auto aurHelperCmd = std::make_unique<CliWrapper>(properties.aurCommand);
      this->aurHelperOut = aurHelperCmd->execute();
    }
    this->updatesNotFetched = false;
  }
}

std::string UpdateChecker::getPendingUpdates(bool forceCheck) {
  if (this->updatesNotFetched || forceCheck) {
    this->checkForUpdates(true);
  }
  if (!checkUpdateOut.empty() || (!aurHelperOut.empty())) {
    std::string finalOut = "There are updates for:\n";
    if (aurHelperOut.empty()) {
      finalOut = finalOut + checkUpdateOut;
    } else if (checkUpdateOut.empty()) {
      finalOut = finalOut + aurHeader + aurHelperOut;
    } else {
      finalOut = finalOut + checkUpdateOut + aurHeader + aurHelperOut;
    }
    auto outputLines = split(finalOut, '\n');
    int lines = 0;
    std::stringstream ss;
    for (auto &outputLine : outputLines) {
      ss << outputLine << '\n';
      lines++;
      if (lines >= properties.maxNumberOfLines) {
        break;
      }
    }
    return ss.str();
  }
  return std::string("");
}

std::vector<std::string> UpdateChecker::split(const std::string &s,
                                              char delimiter) {
  std::vector<std::string> tokens;
  std::string token;
  std::istringstream tokenStream(s);
  while (std::getline(tokenStream, token, delimiter)) {
    tokens.push_back(token);
  }
  return tokens;
}
