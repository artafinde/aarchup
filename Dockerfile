FROM archlinux as base

RUN pacman -Sy --needed --noconfirm \
        base-devel \
        gtk3 \
        git \
        cmake \
        gzip \
        gcc \
        clang \
        perl \
        glibc \
        meson \
        libnotify \
        glib2 \
        ninja

FROM base as builder_gcc
ARG CACHEBUST=1
ENV PATH="${PATH}:/usr/bin/core_perl"
WORKDIR /usr/src
COPY ./ .
RUN mkdir build && \
    meson build && \
    ninja -C build

FROM base as builder_clang
ARG CACHEBUST=1
ENV PATH="${PATH}:/usr/bin/core_perl"
WORKDIR /usr/src
COPY ./ .
RUN mkdir build && \
    CC=clang CXX=clang++ meson build && \
    ninja -C build
