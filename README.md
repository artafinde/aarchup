# Aarchup
[![Build Status](https://gitlab.com/artafinde/aarchup/badges/master/pipeline.svg)](https://gitlab.com/artafinde/aarchup/-/pipelines) [![aur](https://img.shields.io/aur/version/aarchup)](https://aur.archlinux.org/packages/aarchup/)

aarchup (https://gitlab.com/artafinde/aarchup) is a small C++ application which informs the user when system-updates for Archlinux are available.
It is forked from aarchup (https://github.com/aericson/aarchup) and rewritten on C++. It's licenced under the GPLv3.

aarchup uses GTk+ and libnotify to show a desktop notification if updates are available. It follows the unix-philosophy of "just doing one thing, but doing  it  well". It  can be used to regularly check for new updates and show a desktop notification if there are any.

See help `-h|--help` configuration or manpages.

## Thanks

* Rorschach, for the initial work on archup (http://www.nongnu.org/archup)
* aericson, for the spawn of aarchup (https://github.com/aericson/aarchup)

## Bugs

Bugs can be reported in GitHub (https://gitlab.com/artafinde/aarchup)

## Contact

You can contact me with `<artafinde AT gmail DOT com>`
