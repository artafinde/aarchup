project('aarchup', 'cpp',
        version : '2.1.7',
        license : 'GPLv3',
        meson_version : '>=0.55',
        default_options : [
            'cpp_std=c++17',
            'warning_level=3',
            'default_library=static',
        ]
)

cpp = meson.get_compiler('cpp')
add_project_arguments(language : 'cpp')

possible_cpp_flags = []
possible_link_flags = []
if get_option('buildtype') != 'debug'
    possible_cpp_flags += [
        '-ffunction-sections',
        '-fdata-sections',
    ]
    possible_link_flags += '-Wl,--gc-sections'
endif

add_project_arguments(cpp.get_supported_arguments(possible_cpp_flags), language : 'cpp')
add_project_link_arguments(cpp.get_supported_link_arguments(possible_link_flags), language : 'cpp')

libglib = dependency('glib-2.0')
libnotify = dependency('libnotify')
libspdlog = dependency('spdlog', fallback : ['spdlog', 'spdlog_dep'])

pod2man = find_program('pod2man')

libupdatechecker = static_library(
    'libupdatechecker',
    files('''
        src/UpdateChecker.hh
        src/CliWrapper.hh
    '''.split()),
    include_directories : include_directories('src'),
    dependencies : []
)

libnotificationwrapper = static_library(
    'libnotificationwrapper',
    files('''
        src/NotificationWrapper.hh
    '''.split()),
    include_directories : include_directories('src'),
    dependencies : [libnotify]
)

executable(
    'aarchup',
    files('''
        src/UpdateChecker.cc
        src/CliWrapper.cc
        src/NotificationWrapper.cc
        src/aarchup.cpp
    '''.split()),
    include_directories : include_directories('src'),
    dependencies : [libspdlog, libnotify],
    link_with : [libupdatechecker, libnotificationwrapper],
    install : true
)

man = custom_target(
    'man',
    output : 'aarchup.1',
    input : 'man/aarchup.1.pod',
    command : [
      pod2man,
      '--section=1',
      '--center=Aarchup Manual',
      '--name=AARCHUP',
      '--release=Aarchup @0@'.format(meson.project_version()),
      '@INPUT@', '@OUTPUT@'
    ],
    install : true,
    install_dir : join_paths(get_option('mandir'), 'man1'))

install_data(
  files('''
      extra/aarchup.desktop
      extra/aarchupstartup.sh
  '''.split()),
  install_dir : join_paths(get_option('datadir'), 'doc/aarchup'))

install_data(
  files('''
      extra/archlogo.png
      extra/archlogo.svg
      extra/noxaarchup.sh
  '''.split()),
  install_dir : join_paths(get_option('datadir'), 'aarchup'))

install_data(
  files('''
      extra/aarchup.timer
      extra/aarchup.service
  '''.split()),
  install_dir : join_paths(get_option('prefix'), 'lib/systemd/user'))

run_target(
  'fmt',
  command : [
    join_paths(meson.source_root(), 'build-aux/clang-format')
  ]
)
